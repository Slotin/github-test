import { applyMiddleware, createStore as reduxCreateStore, Store as ReduxStore, StoreEnhancer } from "redux";
import thunk, { ThunkAction } from "redux-thunk";
import rootReducer, { RootState } from "./reducers/index";

type ThunkAction2<R> = ThunkAction<R, RootState, void, any>;
export type ThunkActionCreator<R> = (...args: any[]) => ThunkAction2<R>;
export type Store = ReduxStore<RootState>;

function applyDevTools( storeEnhancer: StoreEnhancer<{ dispatch: any }> ) {
  const devToolsComposeEnhancerKey: string = "__REDUX_DEVTOOLS_EXTENSION_COMPOSE__";
  const devToolsComposeEnhancer = window[devToolsComposeEnhancerKey];
  if (devToolsComposeEnhancer) {
    return devToolsComposeEnhancer(storeEnhancer);
  } else {
    return storeEnhancer;
  }
}

export default function createStore(initialState?: RootState): Store {
  // @ts-ignore
  return reduxCreateStore<RootState>(
    rootReducer,
    initialState!,
    applyDevTools(applyMiddleware(thunk))
  );
}