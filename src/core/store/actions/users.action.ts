import { Dispatch } from "redux";

import { getUsers } from "@api";
import { TUserListItem } from "@models";
import { RootState } from "@reducers"


export const enum UsersActions {
  LoadUsers = "[Users] Load users slots",
  LoadUsersSuccess = "[Users] Load users success",
  LoadUsersFail = "[Users] Load users failure",
  ClearUsersCache = "[Users] clear users cache",
}

export type TUsersAction = {
  type: UsersActions
};

export type TLoadUsers = TUsersAction & {
  payload: null
};

export type TLoadUsersFromRemote = TUsersAction & {
  payload: null
};

export type TLoadUsersSuccess = TUsersAction & {
  payload: TUserListItem[]
};

export type TLoadUsersFail = TUsersAction & {
  payload: Error;
}

export type TClearUsersCache = TUsersAction & {
  payload: null
}

export type TUsersActions =
  & TLoadUsers
  & TLoadUsersSuccess
  & TLoadUsersFail
  & TClearUsersCache;

export const loadUsers = () => async (dispatch: Dispatch<any>, getState: () => RootState) => {
  dispatch({
    type: UsersActions.LoadUsers
  });

  const since = getState().users.lastLoadedUser

  try {
    const users:TUserListItem[] = await getUsers(since);
    dispatch({
      payload: users,
      type: UsersActions.LoadUsersSuccess
    })
  } catch (e) {
    dispatch({
      payload: e,
      type: UsersActions.LoadUsersFail
    })
  }

}
