import { combineReducers } from "redux";


import { TUsersState, usersReducer } from "./users.reducer"

export type RootState = {
  users: TUsersState
}

const rootReducer = combineReducers<RootState>({
  users: usersReducer
});

export default rootReducer;