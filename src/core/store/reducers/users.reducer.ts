import produce from "immer";
import { last } from "lodash";

import { TUsersActions, UsersActions } from "@actions"
import { TUserListItem } from "@models";
import { Maybe } from "@types"

export type TUsersState = {
  list: TUserListItem[];
  isLoading: boolean;
  lastLoadedUser: number;
  isError: boolean;
  hasMore: boolean;
  error: Maybe<Error>;
}

const initialUsersState = {
  error: null,
  hasMore: true,
  isError: false,
  isLoading: false,
  lastLoadedUser: 0,
  list: [],
};

export const usersReducer = (
  state: TUsersState = initialUsersState,
  {type, payload}: TUsersActions
) => {
  return produce(state, (draft: TUsersState) => {
    switch ( type ) {
      case UsersActions.LoadUsers: {
        draft.isLoading = true;
        break;
      }

      case UsersActions.LoadUsersSuccess: {
        draft.list = state.list.concat(payload);
        draft.isLoading = false;
        const lastUser = last(payload);
        if (lastUser) {
          draft.lastLoadedUser = lastUser.id;
        }
        break;
      }

      case UsersActions.LoadUsersFail: {
        draft.isLoading = false;
        draft.isError = true;
        draft.hasMore = false;
        draft.error = payload;
        break;
      }
    }

    return draft;
  })
};