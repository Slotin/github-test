import axios, { AxiosResponse } from "axios";

import { TUser, TUserListItem } from "@models"
import { Maybe } from "@types";

import { baseUrl } from "../config"


function buildSinceUsersParams(since: Maybe<number>) {
  if (since) {
    return { since }
  } else {
    return { since: 0 }
  }
}

export function getUsers(since: Maybe<number>): Promise<TUserListItem[]> {
  return axios({
    method: 'GET',
    params: buildSinceUsersParams(since),
    url: `${baseUrl}/users`
  }).then((response: AxiosResponse<TUserListItem[]>) => {
    return response.data
  });
}

export function getUserInfo(username:string):Promise<TUser> {
  return axios({
    method: 'GET',
    url: `${baseUrl}/users/${username}`
  }).then((response: AxiosResponse<TUser>) => {
    return response.data
  });
}