import axios, { AxiosResponse } from "axios";

import { TRepositoryItem } from "@models"
import { baseUrl } from "../config"

export function getRepositoriesInfo(user: string) {
  return axios({
    method: 'GET',
    url: `${baseUrl}/users/${user}/repos`
  }).then((response: AxiosResponse<TRepositoryItem[]>) => {
    return response.data
  });
}