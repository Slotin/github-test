export type TUserListItem = {
  login: string,
  id: number,
  node_id: string,
  avatar_url: string,
}

export type TUser = TUserListItem & {
  name: string
}