import { Maybe } from "@types";

export type TRepositoryItem = {
  name: string;
  description: string;
  open_issues: number;
  language: Maybe<string>
}