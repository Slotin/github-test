import * as React from "react";
import { Provider } from "react-redux";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { Store } from "redux";

import "./index.css";

import Header from "./header";

import Users from "@users/index";
import Repositories from "repositories/index";

import { RootState } from "@reducers";

type Props = { store: Store<RootState>; };

const RedirectComponent = ():any => (<Redirect to="/users" />);

class Index extends React.Component<Props> {
  public render() {
    return (
      <Provider store={this.props.store}>
        <BrowserRouter>
          <div className="App">
            <React.StrictMode>
              <Header/>
              <main className="App__content__pageWrapper">
                <Switch>
                  {/* TODO: Mark routes as Authenticated/Unauthenticated */}
                  <Route exact={true} path="/users" component={Users} />
                  <Route path="/repositories/:login" component={Repositories} />
                  <Route component={RedirectComponent} />
                </Switch>
              </main>
            </React.StrictMode>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default Index;
