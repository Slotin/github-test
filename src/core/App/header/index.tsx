import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import * as React from "react";
import logo from "./xite_mail_logo.png";

import "./style.css";

class Header extends React.Component {
  public render() {
    return (
      <div className="App-header">
        <AppBar position="static" className="App-header__app-bar">
          <IconButton
            aria-label="Menu"
            className="App-header__app-bar__icon-button"
          >
            <img src={logo} className="App-header__app-bar__icon-button__logo" alt="logo" />
          </IconButton>
        </AppBar>
      </div>
    )
  }
}

export default Header;