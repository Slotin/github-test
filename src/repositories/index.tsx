import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom"

import CircularProgress from "@material-ui/core/CircularProgress";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import { getRepositoriesInfo } from "@api";
import { TRepositoryItem } from "@models";

import BackButton from "./components/backButton";
import RepositoryItem from "./components/repositoryItem";

import "./style.css";

type Props = RouteComponentProps<{login: string}>
type State = {
  repositories: TRepositoryItem[];
  isLoading: boolean
}

class Repositories extends React.Component<Props, State> {
  public state = {
    isLoading: false,
    repositories: [],
  }
  public componentDidMount() {
    this.setState({ isLoading: true }, () => {
      getRepositoriesInfo(this.props.match.params.login).then(repositories => {
        this.setState({ repositories, isLoading: false });
      }).catch(err => {
        this.setState({ isLoading: false });
      });
    });
  }

  public renderSpinner = () => (
    <CircularProgress
      color="primary"
      className="Repositories__content__progress"
      size={50}
    />
  )

  public renderTable = () => (
    <Table className="Repositories__content__table">
      <TableHead>
        <TableRow>
          <TableCell key={0}>Repository name</TableCell>
          <TableCell key={1}>Description</TableCell>
          <TableCell key={2}>Number of open issues</TableCell>
          <TableCell key={3}>Language</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {this.state.repositories.map((item: TRepositoryItem, index) => (
          <RepositoryItem repositoryItem={item} key={index}/>
        ))}
      </TableBody>
    </Table>
  )

  public render() {
    return (
      <div className="Repositories" id="Repositories__id">
        <BackButton
          className="Repositories__back-button"
          handleClick={this.handleBackButtonClick}
        />
        <div className="Repositories__content">
          {this.state.isLoading && this.renderSpinner()}
          {!this.state.isLoading && this.renderTable()}
        </div>
      </div>
    );
  }

  private handleBackButtonClick = () => {
    this.props.history.push('/users')
  }
}

export default withRouter(Repositories);
