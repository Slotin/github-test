import Button from "@material-ui/core/Button"
import BackIcon from "@material-ui/icons/KeyboardArrowLeft"
import * as React from "react";

import "./style.css";

type Props = {
  className: string;
  handleClick():void;
}

export default class BackButton extends React.Component <Props> {
  public render() {
    return (
      <Button
        className={`BackButton ${this.props.className}`}
        onClick={this.props.handleClick}
      >
        <BackIcon/>
      </Button>
    );
  }
}