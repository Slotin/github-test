import TableCell from "@material-ui/core/TableCell"
import TableRow from "@material-ui/core/TableRow"
import * as React from "react";

import { TRepositoryItem } from "@models";

import "./style.css"

type Props = {
  repositoryItem: TRepositoryItem
}

export default class RepositoryItem extends React.Component<Props> {
  public render() {
    const { repositoryItem } = this.props;
    return (
      <TableRow className="RepositoryItem">
        <TableCell key="repository_item_name">{repositoryItem.name}</TableCell>
        <TableCell
          key="repository_item_description"
          className="RepositoryItem__description"
        >{repositoryItem.description}</TableCell>
        <TableCell key="repository_item_issues">{repositoryItem.open_issues}</TableCell>
        <TableCell key="repository_item_language">{repositoryItem.language ? repositoryItem.language : ''}</TableCell>
      </TableRow>
    );
  }
}