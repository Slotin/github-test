import { connect } from "react-redux";
import { withRouter } from "react-router";
import { Dispatch } from "redux";

import { loadUsers } from "@actions"
import { TUserListItem } from "@models";


import { RootState } from "@reducers"
import UsersComponent from "./mainComponent";

export type UsersStateToProps = {
  users: TUserListItem[];
  hasMore: boolean;
  isLoading: boolean;
  lastLoaded: number;
};

export type UsersDispatchToProps = {
  loadUsers(): void
}

function mapStateToProps(state: RootState): UsersStateToProps {
  return {
    hasMore: state.users.hasMore,
    isLoading: state.users.isLoading,
    lastLoaded: state.users.lastLoadedUser,
    users: state.users.list
  };
}

function mapDispatchToProps (dispatch: Dispatch<any>): UsersDispatchToProps {
  return {
    loadUsers: () => dispatch(loadUsers()),
  };
}

export default withRouter(
  // @ts-ignore
  connect<UsersStateToProps, UsersDispatchToProps>(mapStateToProps, mapDispatchToProps)(UsersComponent));