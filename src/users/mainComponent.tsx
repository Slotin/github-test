import List from "@material-ui/core/List";
import * as React from "react";
import * as InfiniteScroll from "react-infinite-scroll-component";

import { RouteComponentProps } from "react-router-dom";

import { UsersDispatchToProps, UsersStateToProps } from "./index"
import "./style.css";

import UserItem from "./components/userItem";

type Props = UsersStateToProps & UsersDispatchToProps & RouteComponentProps<null>;

class Users extends React.Component<Props> {
  public componentDidMount() {
    if (this.props.users.length === 0) {
      this.props.loadUsers();
    }
  }

  public render() {
    const { users } = this.props
    return (
      <div
        className="Users"
        id="scrollableDiv"
      >
        <InfiniteScroll
          dataLength={users.length}
          next={this.loadMoreRows}
          hasMore={this.props.hasMore}
          loader={<div className="loader" key={0}>Loading ...</div>}
          scrollableTarget="scrollableDiv"
          endMessage={
            <p style={{textAlign: 'center'}}>
              <b>Yay! You have seen it all</b>
            </p>
          }
        >
          <List className="Users__list">
          {users.map((item, index) => (
            <UserItem key={index} user={item} handleUserClick={this.handleUserClick}/>
          ))}
          </List>
        </InfiniteScroll>
      </div>
    );
  }

  private loadMoreRows = () => {
    this.props.loadUsers();
  }

  private handleUserClick = (login: string) => {
    this.props.history.push(`/repositories/${login}`)
  }
}

export default Users;
