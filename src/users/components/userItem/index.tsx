import * as React from "react";

import { TUserListItem } from "@models";

import Avatar from "@material-ui/core/Avatar";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";

import "./style.css";

type Props = {
  user: TUserListItem,
  handleUserClick(login: string): void
}

export default class UserItem extends React.Component<Props> {
  public render() {
    const { user } = this.props;
    return (
      <ListItem
        className="UsersListItem"
        onClick={this.props.handleUserClick.bind(this, user.login)}>
        <ListItemIcon className="UsersListItem__icon">
          <Avatar className="UsersListItem__icon__image" src={user.avatar_url}/>
        </ListItemIcon>
        <ListItemText className="UsersListItem__text">{user.login}</ListItemText>
      </ListItem>
    )
  }
}