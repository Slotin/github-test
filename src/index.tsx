import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./core/App/index";
import registerServiceWorker from "./registerServiceWorker";
import "./styles/index.scss";

import createStore from "./core/store/index";
const store = createStore();

ReactDOM.render(
  <App store={store} />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
