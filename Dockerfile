FROM node:8.11.3-jessie

RUN mkdir -p /mnt/Code
COPY ./ /mnt/Code

WORKDIR /mnt/Code
RUN npm install

EXPOSE 3000

CMD ["npm", "run", "start"]
